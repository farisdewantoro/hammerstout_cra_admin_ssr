export default {
    loginJwt: {
        secretOrPrivateKey: "X5xUjKdxxx4-.9kbcuqwzxc--23kv,df594.41239zsc92231",
        expiresIn: '1d'
    },
    session: {
        cookieKey: "iCtkGVe0-15asd12315sax-xJkLm2K2220XlkD",
        name: "hammerstout_s",
        secret: "iCtkGVe0-4FKIGgBopL2QUM9K-jIK9miZhQExxxx2xuxxxx",
        maxAge: 24 * 60 * 60 * 1000
    },
    jwt: {
        secretOrPrivateKey: "AAAABB3L-X59kbcuqwzxc--23kv,df594.41239zsc92231",
        secretOrPrivateKey2: "2DCtkGVe-jifmqs53v6sbgg05u3fkbcDuDxDDqwzD2xc--23kv,df594.0ut79s41qi0lhg",
        secretOrPrivateKey3: "X59kbcuqwzxc-VcMFhXkPjVaIjz--23kv,df594.df594xxx",
        expiresIn: '2d'
    },
    origin: {
        url: "https://hammerstoutdenim.com",
        datafeed: "http://hammerstoutdenim.com"
    },
    media: {
        url: "http://admin.hammerstoutdenim.com/media/"
    },
    passport: {
        secretOrPrivateKey: "xg-.ff2dx--23kv,dax2DD.41239zsc92231",
    },
    rajasms: {
        key: "457f38ca6a3265ff7ee62df327d70854",
        username: "hammerstoutdenim"
    },
    midtrans: {
        url: "https://api.midtrans.com",
        id: "G592798515",
        clientKey: "Mid-client-Wn88s0ecuXz5VQoE",
        serverKey: "Mid-server-JKiiGZJ6mH39aIrEZGCLjWWA",
        isProduction: true
    },
    rajaongkir: {
        key: "850129b7327c8206f3875333eb281f0e",
        originId: 23,
        name: "bandung",
    },
    database: {
        host: 'localhost',
        user: 'hammerst_denim',
        password: '6!5jat!e8iLQ',
        database: 'hammerst_hammer',
    },
    instagram: {
        access_token: '4349263588.1677ed0.2961079b3f7e4edea6130da9e50dd3fd',
        clientID: '02fb6221724249ef9df4af191679b6f9',
        clientSecret: 'ef08d2d4b38a47939d6ebd7167ba49e6',
        redirect: 'http://localhost:5000/api/instagram/refresh/token'
    },
}